
<?php include './section/slider_content.php';?>
<!-- HIGHLIGHT CTA  -->   

<!--  SECTION DONATION PROCESS -->
<?php include './section/donation_process.php';?>
 <!--  end .section-process --> 

<!--  SECTION COUNTER   -->
<?php include './section/counter_content.php';?>
<!--  end .section-counter -->

<!--  SECTION APPOINTMENT   -->
<?php include './section/appointment_content.php';?>
<!--  end .appointment-section  -->

<!-- SECTION TESTIMONIAL   -->
<?php include './section/testtimonial_content.php';?>
<!--  end .section-client-testimonial --> 

<!-- SECTION TEAM  -->
<?php include './section/volunteers_content.php';?>
<!--  end .section-our-team -->  

<!-- SECTION CTA -->

<!-- end cta-section  -->  
<?php include './section/join_us.php';?>
<!--  SECTION CAMPAIGNS   -->
<?php include './section/home_page_campain.php';?>
<!--  SECTION GALLERY  -->
<!-- end .section-content-block  -->
<?php include './section/home_page_gallery.php';?>
<!-- SECTION LOGO   -->

<!--  end .section-client-logo -->
<?php include './section/sponsor_content.php';?>
<!-- SECTION BLOG   -->
<?php include './section/home_page_blog.php';?> 
<!--  end .section-latest-blog -->