<header class="main-header clearfix" data-sticky_header="true">
    <div class="top-bar clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-12">
                    <p>Welcome to blood donation center.</p>
                </div>
                <div class="col-md-4col-sm-12">
                    <div class="top-bar-social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-youtube"></i></a>
                    </div>   
                </div> 
            </div>
        </div> <!--  end .container -->
    </div> <!--  end .top-bar  -->
    <section class="header-wrapper navgiation-wrapper">
        <div class="navbar navbar-default">			
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="logo" href="index.php"><img alt="" src="images/logo.png"></a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="index.php" title="Home">Home</a></li>
                        <li><a href="./find_donor.php" title="Find Donor">Find Donor</a></li>
                        <li><a href="./blood_request.php" title="Blood Request">Blood Request</a></li>
                        <li>
                            <a href="#">Campaign</a>
                            <ul class="drop-down">
                                <li><a href="./all_campagn.php">All Campaigns</a></li>
                                <li><a href="./single_campaign.php">Single Campaign</a></li>
                                <li><a href="#">Service Organization</a></li>
                            </ul>
                        </li>
                        <li><a href="./gallery.php" title="Gallery">Gallery</a></li>
              
                        <li><a href="#" title="Blog">Blog</a></li>
                        <li><a href="#" title="About Us">About Us</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</header>