<?php
    $slider = $ob_app->select_slider_info();
?>
<div class="slider-wrap">
    <div id="slider_1" class="owl-carousel owl-theme">
        <?php
        foreach ($slider as $value) { ?>
        <div class="item">
            <img src="admin/slider_image/<?php echo $value['slider_image']; ?>" alt="img">
        </div>
        <?php }?>
    </div>

</div>