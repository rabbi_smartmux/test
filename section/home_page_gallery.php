<?php
$gallery = $ob_app->select_all_gallery_image_info();
?>
<section class="section-content-block no-bottom-padding section-secondary-bg">
    <div class="container">
        <div class="row section-heading-wrapper">
            <div class="col-md-12 col-sm-12 text-center">
                <h2 class="section-heading">Photo Gallery</h2>
                <p class="section-subheading">Campaign photos of different parts of world and our prestigious voluntary work</p>
            </div> <!-- end .col-sm-10  -->                      
        </div> <!-- end .row  -->
    </div> <!--  end .container -->
    <div class="container-fluid wow fadeInUp">
        <div class="row no-padding-gallery">
            <?php foreach ($gallery as $value) { ?>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 gallery-container">
                <a class="gallery-light-box"  data-gall="myGallery" href="admin/gallery_image/<?php echo $value['gallery_image'];?>">
                    <figure class="gallery-img">
                        <img src="admin/gallery_image/<?php echo $value['gallery_image'];?>" alt="gallery image" />
                    </figure> <!-- end .gallery-img  -->
                </a>
            </div><!-- end .col-sm-3  -->
            <?php }?>     
        </div> <!-- end .row  -->
    </div><!-- end .container-fluid  -->

</section>