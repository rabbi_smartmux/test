<?php
$result = $ob_app->select_donation_organization_info();
?>
<section class="section-appointment">
    <div class="container wow fadeInUp">
        <div class="row">
            <div class="col-lg-6 col-md-6 hidden-sm hidden-xs"> 
                <figure class="appointment-img">
                    <img src="images/appointment.jpg" alt="appointment image">
                </figure>
            </div> <!--  end col-lg-6  -->
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                <div class="appointment-form-wrapper text-center clearfix">
                    <h3 class="join-heading">Request Appointment</h3>
                    <?php
                        if (isset($_POST['btn'])) {
                            $message = $ob_app->save_appoinment_info($_POST);
                        }
                        ?>
                    <form method="POST" class="appoinment-form"> 
                        <div class="form-group col-md-6">
                            <input id="your_name" name="name" class="form-control" placeholder="Name" type="text" required="">
                        </div>
                        <div class="form-group col-md-6">
                            <input id="your_email" name="email_address" class="form-control" placeholder="Email" type="email">
                        </div>
                        <div class="form-group col-md-6">
                            <input id="your_phone" name="mobile_number" class="form-control" placeholder="Phone" type="text" required="">
                        </div>
                        <div class="form-group col-md-6">
                            <div class="select-style">                                    
                                <select class="form-control" name="organization_id" required="">
                                    <option>Donation Center</option>
                                   <?php foreach ($result as $value) {?>
                                    <option value="<?php echo $value['id'];?>"><?php echo $value['organization_name'];?></option>
                                   <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            
                            <input id="datepicker" name="appoint_date" class="form-control"  placeholder="Date" type="text" required="">
                        </div>
                        <div class="form-group col-md-6">
                            <input id="your_time" name="appoint_time" class="form-control" placeholder="Time" type="time">
                        </div>
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <textarea style="margin-top: 10px" id="textarea_message" name="content" class="form-control" rows="4" placeholder="Your Message..."></textarea>
                        </div>         
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <button id="btn_submit" class="btn-submit" name="btn" type="submit">Get Appointment</button>
                        </div>
                    </form>
                </div> <!-- end .appointment-form-wrapper  -->
            </div> <!--  end .col-lg-6 -->
        </div> <!--  end .row  -->
    </div> <!--  end .container -->
</section>