<?php
    $opnion = $ob_app->select_donor_opinion_info();
?>
<section class="section-content-block section-client-testimonial">
    <div class="container"> 
        <div class="testimonial-container text-center">
            <?php foreach ($opnion as $value) { ?>
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="testimony-layout-1">
                    <h3 class="people-quote"><?php echo $value['opinion_title'];?></h3>
                    <p class="testimony-text">
                        <i class="fa fa-quote-left" aria-hidden="true"></i>
                        <?php echo $value['user_opinion'];?>
                        <i class="fa fa-quote-right" aria-hidden="true"></i>
                    </p>
                    <h6><?php echo $value['user_name'];?></h6>
                    <span><?php echo $value['user_address'];?></span>
                </div> <!-- end .testimony-layout-1  -->
            </div> <!--  end col-md-10  -->
            <?php }?>
            
            
        </div>  <!--  end .row  -->   
    </div> <!-- end .container  -->
</section>