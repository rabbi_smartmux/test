<?php
$result = $ob_app->select_donation_process_info();
?>
<section class="section-content-block section-process">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-sm-12 text-center">
                <h2 class="section-heading"><span>Donation</span> Process</h2>
                <p class="section-subheading">The donation process from the time you arrive center until the time you leave</p>
            </div> <!-- end .col-sm-10  -->                    

        </div> <!--  end .row  -->

        <div class="row wow fadeInUp">
         <?php foreach ($result as $value) { ?>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="process-layout">
                    <figure class="process-img">
                        <img src="admin/donationPic/<?php echo $value['donationPic'];?>" alt="process" />
                        <div class="step">
                            <h3>1</h3>
                        </div>
                    </figure> <!-- end .process-img  -->
                    <article class="process-info">
                        <h2><?php echo $value['title'];?></h2>   
                        <p><?php echo $value['content'];?></p>
                    </article>
                </div> <!--  end .process-layout -->
            </div> <!--  end .col-lg-3 -->
         <?php }?>

             <!--  end .col-lg-3 -->

        </div> <!--  end .row --> 

    </div> <!--  end .container  -->

</section>