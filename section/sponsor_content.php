<?php
$sponsor = $ob_app->select_all_sponsor_image_info();
?>
<section class="section-client-logo">

    <div class="container wow fadeInUp">

        <div class="row section-heading-wrapper">

            <div class="col-md-12 col-sm-12 text-center">
                <h2 class="section-heading">Our Sponsors</h2>
                <p class="section-subheading">The sponsors who give their valuable amount to fulfill our mission.</p>
            </div> <!-- end .col-sm-10  -->

        </div> <!-- end .row  -->


        <div class="row">

            <div class="logo-items logo-layout-1 text-center">
              <?php foreach ($sponsor as $value) {
                ?>
                <div class="client-logo">

                    <img src="admin/sponsor_image/<?php echo $value['sponsor_image'];?>" alt="" />

                </div>

                <!-- <div class="client-logo">

                    <img src="images/logo_2.jpg" alt="" />

                </div>


                <div class="client-logo">

                    <img src="images/logo_3.jpg" alt="" />

                </div>

                <div class="client-logo">

                    <img src="images/logo_4.jpg" alt="" />

                </div>

                <div class="client-logo">

                    <img src="images/logo_5.jpg" alt="" />

                </div>


                <div class="client-logo">

                    <img src="images/logo_6.jpg" alt="" />

                </div>

                <div class="client-logo">

                    <img src="images/logo_7.jpg" alt="" />

                </div>

                <div class="client-logo">

                    <img src="images/logo_8.jpg" alt="" />

                </div> -->
                <?php }?>
            </div> <!-- end .logo-items  -->


        </div> <!-- end row  -->

    </div> <!-- end .container  -->

</section>
