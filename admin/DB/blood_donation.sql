-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2018 at 05:36 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blood_donation`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_appoinment_request`
--

CREATE TABLE `tbl_appoinment_request` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email_address` varchar(50) NOT NULL,
  `mobile_number` varchar(50) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `appoint_date` varchar(50) NOT NULL,
  `appoint_time` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_appoinment_request`
--

INSERT INTO `tbl_appoinment_request` (`id`, `name`, `email_address`, `mobile_number`, `organization_id`, `appoint_date`, `appoint_time`, `content`, `deletion_status`) VALUES
(1, 'Jakir Hossain', 'jhossain.cse@gmail.com', '01742108943', 1, '2018-01-16', '03:33', 'This is an Appoinment', 0),
(2, 'Abir Hasan', 'abir@gmail.com', '016805689796', 1, '2018-01-25', '10:00', 'I want to donate Blood', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blood_collection`
--

CREATE TABLE `tbl_blood_collection` (
  `id` int(11) NOT NULL,
  `donor_id` int(11) NOT NULL,
  `blood_group` varchar(100) NOT NULL,
  `blood_amount` varchar(100) NOT NULL,
  `donation_date` varchar(100) NOT NULL,
  `donation_time` varchar(100) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `deletion_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blood_collection`
--

INSERT INTO `tbl_blood_collection` (`id`, `donor_id`, `blood_group`, `blood_amount`, `donation_date`, `donation_time`, `organization_id`, `deletion_status`) VALUES
(1, 1, 'B+', '1 Pack', '11-01-2018', '09:30', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blood_donor`
--

CREATE TABLE `tbl_blood_donor` (
  `id` int(11) NOT NULL,
  `donor_name` varchar(100) NOT NULL,
  `father_name` varchar(100) NOT NULL,
  `mother_name` varchar(100) NOT NULL,
  `current_address` text NOT NULL,
  `permanent_address` text NOT NULL,
  `dob` varchar(100) NOT NULL,
  `email_address` varchar(50) NOT NULL,
  `city_id` int(11) NOT NULL,
  `nID` varchar(50) NOT NULL,
  `phone_number` varchar(50) NOT NULL,
  `varsity_name` int(11) NOT NULL,
  `mobile_number` varchar(50) NOT NULL,
  `alternative_number` varchar(50) NOT NULL,
  `donorImages` varchar(150) NOT NULL,
  `blood_group` varchar(100) NOT NULL,
  `registration_date` varchar(100) NOT NULL,
  `location_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blood_donor`
--

INSERT INTO `tbl_blood_donor` (`id`, `donor_name`, `father_name`, `mother_name`, `current_address`, `permanent_address`, `dob`, `email_address`, `city_id`, `nID`, `phone_number`, `varsity_name`, `mobile_number`, `alternative_number`, `donorImages`, `blood_group`, `registration_date`, `location_id`, `status`, `deletion_status`) VALUES
(1, 'Fazle Elahi Fahad', 'Test', 'Test', 'Dhaka', 'Dhaka', '10-09-1994', 'fazle@gmail.com', 1, '1994,0910,2500,12,559', '01258963', 1, '01717495598', '01680568796', '1515607963.jpg', 'B+', '10-01-2018', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blood_group`
--

CREATE TABLE `tbl_blood_group` (
  `id` int(11) NOT NULL,
  `blood_group` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blood_group`
--

INSERT INTO `tbl_blood_group` (`id`, `blood_group`, `status`, `deletion_status`) VALUES
(1, 'A+', 1, 0),
(2, 'A-', 1, 0),
(3, 'B+', 1, 0),
(4, 'B-', 1, 0),
(5, 'AB+', 1, 0),
(6, 'AB-', 1, 0),
(7, 'O+', 1, 0),
(8, 'O-', 1, 0),
(9, 'A1+', 1, 0),
(10, 'A1-', 1, 0),
(11, 'A1B+', 1, 0),
(12, 'A1B-', 1, 0),
(13, 'A2+', 1, 0),
(14, 'A2-', 1, 0),
(15, 'A2B+', 1, 0),
(16, 'A2B-', 1, 0),
(17, 'Bombay Blood Group', 0, 0),
(19, 'INRA', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blood_request`
--

CREATE TABLE `tbl_blood_request` (
  `id` int(11) NOT NULL,
  `patient_name` varchar(100) NOT NULL,
  `blood_group` varchar(100) NOT NULL,
  `patient_age` varchar(20) NOT NULL,
  `blood_qty` varchar(100) NOT NULL,
  `mobile_number` varchar(100) NOT NULL,
  `line_number` varchar(50) NOT NULL,
  `email_address` varchar(50) NOT NULL,
  `hispital_name` varchar(150) NOT NULL,
  `location_id` int(11) NOT NULL,
  `present_address` text NOT NULL,
  `purpose` varchar(100) NOT NULL,
  `need_date` varchar(100) NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blood_request`
--

INSERT INTO `tbl_blood_request` (`id`, `patient_name`, `blood_group`, `patient_age`, `blood_qty`, `mobile_number`, `line_number`, `email_address`, `hispital_name`, `location_id`, `present_address`, `purpose`, `need_date`, `deletion_status`) VALUES
(1, 'Ariful Islam', 'O+', '24', '2 packet', '8801742108943', '82258552', 'jhossain.cse@gmail.com', 'Labaid', 1, 'Mirpur', 'Cancer', '22-01-2018', 0),
(2, 'Ariful Islam', 'O+', '24', '2 packet', '8801742108943', '82258552', 'jhossain.cse@gmail.com', 'Labaid', 1, 'Mirpur', 'Cancer', '22-01-2018', 0),
(3, 'Sajal Dash', 'O-', '25', '2 packet', '8801742108943', '82258552', 'jhossain.cse@gmail.com', 'Labaid', 2, 'Mirpur', 'Cancer', '25-01-2018', 0),
(4, 'Sajal Dash', 'O-', '25', '2 packet', '8801742108943', '82258552', 'jhossain.cse@gmail.com', 'Labaid', 2, 'Mirpur', 'Cancer', '25-01-2018', 0),
(5, 'Probal Sha', 'AB-', '23', '1 Paket', '8801558965359', '82258552', 'shefaclinic@gmail.com', 'Bardem', 2, 'Mirpur', 'Faver', '24-01-2018', 0),
(6, 'Fazla Elahi Fahad', 'AB+', '22', '1 Paket', '8801955218927', '554589896565', 'arifulislam@gmail.com', 'Popular', 4, 'Mirpur', 'Tayford', '19-01-2018', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blood_seeker`
--

CREATE TABLE `tbl_blood_seeker` (
  `id` int(11) NOT NULL,
  `seeker_name` varchar(100) NOT NULL,
  `father_name` varchar(100) NOT NULL,
  `mother_name` varchar(100) NOT NULL,
  `current_address` text NOT NULL,
  `permanent_address` text NOT NULL,
  `dob` varchar(100) NOT NULL,
  `email_address` varchar(50) NOT NULL,
  `nID` varchar(20) NOT NULL,
  `phone_number` varchar(50) NOT NULL,
  `current_job_location` varchar(100) NOT NULL,
  `mobile_number` varchar(50) NOT NULL,
  `certificated_from` varchar(50) NOT NULL,
  `donorPic` varchar(100) NOT NULL,
  `blood_group` varchar(100) NOT NULL,
  `registration_date` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blood_seeker`
--

INSERT INTO `tbl_blood_seeker` (`id`, `seeker_name`, `father_name`, `mother_name`, `current_address`, `permanent_address`, `dob`, `email_address`, `nID`, `phone_number`, `current_job_location`, `mobile_number`, `certificated_from`, `donorPic`, `blood_group`, `registration_date`, `status`, `deletion_status`) VALUES
(1, 'Sajal Das', 'Test', 'Test', 'House : 517/2, West-Shawrapara,Mirpur-10,Dhaka-1216', 'Vill. Jhikerhati,Post. Ghotmajhi, District . Madaripur , Division . Dhaka', '10-09-1994', 'sajal@gmal.com', '1994,2015,8942,31,55', '1258', '02589', '01742108943', '01558965359', '1515524791.jpg', 'B+', '10-01-2018', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_campaign`
--

CREATE TABLE `tbl_campaign` (
  `id` int(11) NOT NULL,
  `even_name` varchar(100) NOT NULL,
  `even_title` varchar(100) NOT NULL,
  `even_venue` text NOT NULL,
  `campaign_image` text NOT NULL,
  `even_date` varchar(100) NOT NULL,
  `even_time` varchar(100) NOT NULL,
  `even_creator` varchar(100) NOT NULL,
  `creator_images` text NOT NULL,
  `creator_opinion` text NOT NULL,
  `even_description` text NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_campaign`
--

INSERT INTO `tbl_campaign` (`id`, `even_name`, `even_title`, `even_venue`, `campaign_image`, `even_date`, `even_time`, `even_creator`, `creator_images`, `creator_opinion`, `even_description`, `deletion_status`) VALUES
(1, 'International Blood Donation', 'International Blood Donation Day-2017', 'Bongo Bondhu Sommelon Kendro', '1516388404.jpg', '20-01-2018', '10:00', 'Fazle Elahi Fahad', '1516388404.jpg', 'Sydney â€“ the fourth most desirable student city in the world â€“ is home to Macquarie University, a globally ranked top 200 university which offers a world-class education.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis necessitatibus magni optio nobis dicta esse quas molestias deserunt et, sint fuga debitis asperiores dolorum illum soluta quae eos itaque nemo.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis tenetur nulla consequatur. Facere eaque quod assumenda dolorem beatae, nulla et rem quisquam possimus vitae, commodi optio sunt fugit? Illo, itaque debitis. Amet, ex pariatur dolores cupiditate provident recusandae veritatis voluptatibus velit eius quos impedit nulla saepe iste dolor assumenda reprehenderit laborum itaque id. Voluptatum sit ipsam provident officiis, aspernatur aliquid.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa doloremque incidunt ad nobis quod natus, repudiandae suscipit iste error laudantium aperiam, quae, eum aspernatur hic facere officiis architecto totam quis ea nostrum consequuntur? Veniam qui ad tempore, dicta obcaecati veritatis eaque voluptatem saepe animi. Magnam nemo dolor excepturi eligendi, repellendus.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_city`
--

CREATE TABLE `tbl_city` (
  `id` int(11) NOT NULL,
  `city_name` varchar(150) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_city`
--

INSERT INTO `tbl_city` (`id`, `city_name`, `status`, `deletion_status`) VALUES
(1, 'Dhaka', 1, 0),
(2, 'Narayagong', 1, 0),
(3, 'Shylet', 1, 0),
(4, 'Rajshahi', 1, 0),
(5, 'Barishal', 1, 0),
(6, 'Chattagram', 1, 0),
(7, 'Madaripur', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_donation_process`
--

CREATE TABLE `tbl_donation_process` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `donationPic` text NOT NULL,
  `content` text NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_donation_process`
--

INSERT INTO `tbl_donation_process` (`id`, `title`, `donationPic`, `content`, `deletion_status`) VALUES
(1, 'REGISTRATION', '1516316529.jpg', 'You need to complete a very simple registration form. Which contains all required contact information to enter in the donation process.', 0),
(2, 'SCREENING', '1516316571.jpg', 'A drop of blood from your finger will take for simple test to ensure that your blood iron levels are proper enough for donation process.', 0),
(3, 'DONATION', '1516316599.jpg', 'After ensuring and passed screening test successfully you will be directed to a donor bed for donation. It will take only 6-10 minutes.', 0),
(4, 'REFRESHMENT', '1516316628.jpg', 'You can also stay in sitting room until you feel strong enough to leave our center. You will receive awesome drink from us in donation zone.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

CREATE TABLE `tbl_gallery` (
  `id` int(11) NOT NULL,
  `gallery_image` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_gallery`
--

INSERT INTO `tbl_gallery` (`id`, `gallery_image`, `status`, `deletion_status`) VALUES
(1, '1516381921.jpeg', 1, 0),
(2, '1516381939.jpg', 1, 0),
(3, '1516382005.jpg', 1, 0),
(4, '1516382070.jpg', 1, 0),
(5, '1516382097.jpg', 1, 0),
(6, '1516382109.jpg', 1, 0),
(7, '1516382123.jpg', 1, 0),
(8, '1516382141.jpg', 1, 0),
(9, '1516382151.jpg', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_location`
--

CREATE TABLE `tbl_location` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `location` text NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_location`
--

INSERT INTO `tbl_location` (`id`, `city_id`, `location`, `deletion_status`) VALUES
(1, 1, 'Mirpur', 0),
(2, 1, 'Dhanmondhi', 0),
(3, 1, 'Banani', 0),
(4, 1, 'Uttara', 0),
(5, 1, 'Azimpur', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_organization`
--

CREATE TABLE `tbl_organization` (
  `id` int(11) NOT NULL,
  `organization_name` text NOT NULL,
  `websiteLink` text NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_organization`
--

INSERT INTO `tbl_organization` (`id`, `organization_name`, `websiteLink`, `deletion_status`) VALUES
(1, 'Blood Donation Club In Bangladesh', 'http://www.blooddonorsbd.com/allbloodbankslist.php', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `id` int(11) NOT NULL,
  `slider_image` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`id`, `slider_image`, `status`, `deletion_status`) VALUES
(1, '1516637957.jpg', 1, 0),
(2, '1516638211.jpg', 1, 0),
(3, '1516638224.jpg', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `profile_pic` text NOT NULL,
  `password` varchar(120) NOT NULL,
  `con_password` varchar(120) NOT NULL,
  `creation_date` varchar(100) NOT NULL,
  `user_type` int(11) NOT NULL COMMENT 'user_type = 1 Super Admin',
  `status` tinyint(4) NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_name`, `full_name`, `profile_pic`, `password`, `con_password`, `creation_date`, `user_type`, `status`, `deletion_status`) VALUES
(1, 'admin', 'Blood Donation', 'profile_pic/jakir.png', '21232f297a57a5a743894a0e4a801fc3', '21232f297a57a5a743894a0e4a801fc3', '02-01-2018', 1, 1, 0),
(2, 'jakir', 'Jakir Hossain', 'profile_pic/7.jpg', '81dc9bdb52d04dc20036dbd8313ed055', '81dc9bdb52d04dc20036dbd8313ed055', '03-01-2018', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_opinion`
--

CREATE TABLE `tbl_user_opinion` (
  `id` int(11) NOT NULL,
  `opinion_title` varchar(100) NOT NULL,
  `user_opinion` text NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_address` varchar(100) NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_opinion`
--

INSERT INTO `tbl_user_opinion` (`id`, `opinion_title`, `user_opinion`, `user_name`, `user_address`, `deletion_status`) VALUES
(1, 'RECIPIENT', 'Do You Feel You Don&rsquo;T Have Much To Offer? You Have The Most Precious Resource Of All: The Ability To Save A Life By Donating Blood! Help Share This Invaluable Gift With Someone In Need.', 'Fazle Rabbi', 'Mirpur-10', 0),
(2, 'DONOR OPINION', 'By the far bridge many a spent cartridge Cheney killed 81 birds that day but in the pear tree survived &hellip;&hellip; the partridge.', 'Sajal Das', 'Dhanmondi', 0),
(3, 'DONOR OPINION', 'Of all writings I love only that which is written with blood. Write with blood: and you will discover that blood is spirit.', 'Friedrich Nietzsche', 'Dhanmondi', 0),
(4, 'DONOR OPINION', 'Young blood must have its course, lad, and every dog its day.', 'Charles Kingsley', 'Mirpur-10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_type`
--

CREATE TABLE `tbl_user_type` (
  `id` int(11) NOT NULL,
  `user_type` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_type`
--

INSERT INTO `tbl_user_type` (`id`, `user_type`, `status`, `deletion_status`) VALUES
(1, 'Super Admin', 1, 0),
(2, 'Admin', 1, 0),
(3, 'Manager', 0, 0),
(4, 'Student', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_varsity_name`
--

CREATE TABLE `tbl_varsity_name` (
  `id` int(11) NOT NULL,
  `varsity_name` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deletion_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_varsity_name`
--

INSERT INTO `tbl_varsity_name` (`id`, `varsity_name`, `status`, `deletion_status`) VALUES
(1, 'University Of Development Alternative (UODA)', 1, 0),
(2, 'University Of Dhaka (DU)', 1, 0),
(3, 'Daffodil International University (DIU) ', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_appoinment_request`
--
ALTER TABLE `tbl_appoinment_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_blood_collection`
--
ALTER TABLE `tbl_blood_collection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_blood_donor`
--
ALTER TABLE `tbl_blood_donor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_blood_group`
--
ALTER TABLE `tbl_blood_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_blood_request`
--
ALTER TABLE `tbl_blood_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_blood_seeker`
--
ALTER TABLE `tbl_blood_seeker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_campaign`
--
ALTER TABLE `tbl_campaign`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_city`
--
ALTER TABLE `tbl_city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_donation_process`
--
ALTER TABLE `tbl_donation_process`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_location`
--
ALTER TABLE `tbl_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_organization`
--
ALTER TABLE `tbl_organization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_user_opinion`
--
ALTER TABLE `tbl_user_opinion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_type`
--
ALTER TABLE `tbl_user_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_varsity_name`
--
ALTER TABLE `tbl_varsity_name`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_appoinment_request`
--
ALTER TABLE `tbl_appoinment_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_blood_collection`
--
ALTER TABLE `tbl_blood_collection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_blood_donor`
--
ALTER TABLE `tbl_blood_donor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_blood_group`
--
ALTER TABLE `tbl_blood_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tbl_blood_request`
--
ALTER TABLE `tbl_blood_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_blood_seeker`
--
ALTER TABLE `tbl_blood_seeker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_campaign`
--
ALTER TABLE `tbl_campaign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_city`
--
ALTER TABLE `tbl_city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_donation_process`
--
ALTER TABLE `tbl_donation_process`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_location`
--
ALTER TABLE `tbl_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_organization`
--
ALTER TABLE `tbl_organization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_user_opinion`
--
ALTER TABLE `tbl_user_opinion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_user_type`
--
ALTER TABLE `tbl_user_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_varsity_name`
--
ALTER TABLE `tbl_varsity_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
