<div id="page-wrapper">
    <?php
    if (isset($pages)) {
        if ($pages == 'user-content') {
            include './setting/user_content.php';
        } else if ($pages == 'user-type-content') {
            include './setting/user_type_content.php';
        } else if ($pages == 'manage-user-type') {
            include './setting/manage_user_type.php';
        } else if ($pages == 'manage-user') {
            include './setting/manage_user_content.php';
        } else if ($pages == 'blood-seeker-reg') {
            include './registration/blood_seeker_registration.php';
        } else if ($pages == 'add-group') {
            include './Information/add_blood_group.php';
        } else if ($pages == 'view-blood-group') {
            include './Information/blood_group.php';
        } else if ($pages == 'add-varsity') {
            include './Information/add_varsity_content.php';
        } else if ($pages == 'view-varsity') {
            include './Information/view_varsity_content.php';
        } else if ($pages == 'blood-donor-reg') {
            include './registration/blood_donor_registration.php';
        } else if ($pages == 'blood-organization') {
            include './Information/service_organization_content.php';
        } else if ($pages == 'blood-collection') {
            include './registration/blood_collection_content.php';
        } else if ($pages == 'appoinment-request') {
            include './userRequest/user_appoinment_request.php';
        } else if ($pages == 'blood-request') {
            include './userRequest/user_blood_request_content.php';
        } else if ($pages == 'add-city') {
            include './Information/add_new_city_content.php';
        } else if ($pages == 'add-location-content') {
            include './Information/add_location_content.php';
        } else if ($pages == 'donation-process') {
            include './website/donation_process_content.php';
        } else if ($pages == 'gallery-content') {
            include './website/gallery_content.php';
        }else if ($pages == 'sponsor-content') {
            include './website/sponsor-content.php';
        } else if ($pages == 'compaign-content') {
            include './website/campaigning_content.php';
        } else if ($pages == 'slider-content') {
            include './website/slider_content.php';
        } else if ($pages == 'donor-opinion') {
            include './website/donor_opinion_content.php';
        } else if ($pages == 'add-volunteers') {
            include './website/add_volunteers_content.php';
        }
    } else {
        require_once './pages/main_page.php';
    }
    ?>
    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2018 &copy; Blood Donation</footer>
</div>
