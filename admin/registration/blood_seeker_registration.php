<?php
$query_result = $obj_user->select_all_blood_group_info();
if (isset($_POST['btn'])) {
    $message = $obj_user->save_blood_seeker_content($_POST);
}
?>
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Blood Seeker</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="home.php">Dashboard</a></li>
                <li><a href="./manage_user.php">Blood Seeker</a></li>
                <li class="active">Add Blood Seeker</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!--.row-->
    <div class="row">
        <div class="col-md-12">
            <span style="font-size: 14px;color: #fff">
                All star marked ( <span style="color:red;">*</span> ) fields are mandatory, please fill up all mandatory fields.
            </span>
            <?php
            if (isset($_SESSION['message'])) {
                ?>          
                <div class="alert alert-success">
                    <a href="#" class="alert-link" style="text-align: center;"><?php echo $_SESSION['message']; ?></a>.
                </div>
                <?php
                unset($_SESSION['message']);
            }
            ?>
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <form method="POST" action="" enctype="multipart/form-data">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputuname">Blood Seeker Name<span style="color: red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="seeker_name" class="form-control" placeholder="Blood Seeker Name"> 
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label for="exampleInputuname">Father Name<span style="color: red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="father_name" class="form-control" placeholder="Father Name"> 
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label for="exampleInputuname">Mother Name</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="mother_name" class="form-control" placeholder="Mother Name"> 
                                    </div>
                                </div>                                 
                                <div class="form-group">
                                    <label for="exampleInputuname">Current Address<span style="color: red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <textarea name="current_address" cols="1" rows="1" class="form-control" placeholder="Current Address"></textarea> 
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top: 34px">
                                    <label for="exampleInputuname">Permanent Address<span style="color: red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <textarea name="permanent_address" cols="1" rows="1" class="form-control" placeholder="Permanent Address"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputpwd1">Date Of Birth<span style="color: red">*</span></label>
                                    <div class="input-group">
                                        <input type="text" name="dob" class="form-control" id="datepicker-autoclose" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputuname">Email Number<span style="color: red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-email"></i></div>
                                        <input type="email" name="email_address" class="form-control" placeholder="example@gmail.com"> </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputuname">Nid Number</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="nID" placeholder="9999,9999,9999,99,999" data-mask="9999,9999,9999,99,999" class="form-control">  
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputuname">Phone Number</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="phone_number" class="form-control" placeholder="Phone Number"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputuname">Mobile Number<span style="color: red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="mobile_number" class="form-control" placeholder="Mobile Number"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputuname">Current Job Location</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="current_job_location" class="form-control" placeholder="Current Job Location"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputuname">Certificated From</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="certificated_from" class="form-control" placeholder="Certificated From"> </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputuname">Profile Picture<span style="color: red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="file" name="donorPic" class="form-control"  placeholder="Profile Picture"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Blood Group<span style="color: red">*</span></label>
                                    <select name="blood_group" class="form-control select2" style="color: #fff">
                                        <option>Select Blood Group</option>
                                        <?php foreach ($query_result as $bloodGroup) { ?>
                                            <option value="<?php echo $bloodGroup['blood_group']; ?>"><?php echo $bloodGroup['blood_group']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputpwd1">Registration Date<span style="color: red">*</span></label>
                                    <div class="input-group">
                                        <input type="text" name="registration_date" class="form-control" id="datepicker-autoclose2" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Status<span style="color: red">*</span></label>
                                    <select name="status" class="form-control select2" style="color: #fff">
                                        <option>Select Status</option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div align="center">
                                <button type="submit" name="btn" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>         
    </div>
</div>