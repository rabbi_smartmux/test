<?php
error_reporting(E_ALL);
$donor = $obj_user->select_all_active_blood_donor();
$org = $obj_user->select_all_service_organization();
$blood_group = $obj_user->select_all_blood_group_info();
if (isset($_POST['btn'])) {
    $message = $obj_user->save_blood_collection_content($_POST);
}
?>
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Blood Collection</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="home.php">Dashboard</a></li>
                <li><a href="./manage_user.php">Blood Collection</a></li>
                <li class="active">Blood Collection</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!--.row-->
    <div class="row">
        <div class="col-md-12">
            <span style="font-size: 14px;color: #fff">
                All star marked ( <span style="color:red;">*</span> ) fields are mandatory, please fill up all mandatory fields.
            </span>
            <?php
            if (isset($_SESSION['message'])) {
                ?>          
                <div class="alert alert-success">
                    <a href="#" class="alert-link" style="text-align: center;"><?php echo $_SESSION['message']; ?></a>.
                </div>
                <?php
                unset($_SESSION['message']);
            }
            ?>
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <form method="POST" action="" enctype="multipart/form-data">
                            <div class="col-md-6">
                                 <div class="form-group">
                                    <label>Donor Name</label>
                                    <select name="donor_id" class="form-control select2" style="color: #fff" required="">
                                        <option>Select Blood Donor</option>
                                        <?php foreach ($donor as $donorName) { ?>
                                            <option value="<?php echo $donorName['id']; ?>"><?php echo $donorName['donor_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div> 
                                <div class="form-group">
                                    <label>Blood Group<span style="color: red">*</span></label>
                                    <select name="blood_group" class="form-control select2" style="color: #fff" required="">
                                        <option>Select Blood Group</option>
                                        <?php foreach ($blood_group as $bloodGroup) { ?>
                                            <option value="<?php echo $bloodGroup['blood_group']; ?>"><?php echo $bloodGroup['blood_group']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                 <div class="form-group">
                                    <label>Blood Amount<span style="color: red">*</span></label>
                                    <select name="blood_amount" class="form-control select2" style="color: #fff" required="">
                                        <option>Select Blood Amount</option>
                                        <option value="1 Pack">1 Pack</option>
                                        <option value="2 Pack">2 Pack</option>
                                        <option value="3 Pack">3 Pack</option>
                                    </select>
                                </div> 
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputpwd1">Donation Date<span style="color: red">*</span></label>
                                    <div class="input-group">
                                        <input type="text" name="donation_date" class="form-control" id="datepicker-autoclose" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputpwd1">Donation Time<span style="color: red">*</span></label>
                                    <div class="input-group clockpicker">
                                        <input type="text" name="donation_time" class="form-control" value="09:30"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Service Organization</label>
                                    <select name="organization_id" class="form-control select2" style="color: #fff">
                                        <option>Select Service Organization</option>
                                        <?php foreach ($org as $org_name) { ?>
                                            <option value="<?php echo $org_name['id']; ?>"><?php echo $org_name['organization_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div> 
                            </div>
                            <div align="center">
                                <button type="submit" name="btn" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>         
    </div>
</div>