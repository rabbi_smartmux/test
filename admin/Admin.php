<?php

class Admin {

//****#####--Database Cunnection Function Start--####***
    public function __construct() {
        $host_name = 'localhost';
        $user_name = 'root';
        $password = '';
        $database_name = 'blood_donation';
        $db_connect = mysqli_connect($host_name, $user_name, $password);

        if ($db_connect) {
            $db_select = mysqli_select_db($db_connect, $database_name);
            if ($db_select) {
                // echo 'Database is Selected';
                return $db_connect;
            } else {
                die("Sorry Database is not selected." . mysqli_error($db_connect));
            }
        } else {
            die("Sorry Database is not connected." . mysqli_error($db_connect));
        }
    }

//****#####--Database Cunnection Function End--####***
//****#####--Common Method Function start--####***
    public function getOneCol($col, $tbl, $comCol, $comVal) {
        $db_connect = $this->__construct();

        $sql = "SELECT $col FROM $tbl WHERE $comCol='$comVal' ";
        if (mysqli_query($db_connect, $sql)) {
            $result = mysqli_query($db_connect, $sql);
            $row = mysqli_fetch_array($result);
            return $row[$col];
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

    public function execute_query($sql) {
        $db_connect = $this->__construct();

        if (mysqli_query($db_connect, $sql)) {
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

    public function fetchRows($sql) {
        $db_connect = $this->__construct();
        $arr = array();
        if (mysqli_query($db_connect, $sql)) {
            $res = mysqli_query($db_connect, $sql);
            while ($row = mysqli_fetch_array($res)) {
                $arr[] = $row;
            }
            return $arr;
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Common Method Function End--####***
    public function getTotalRows($sql) {
        $db_connect = $this->__construct();

        if (mysqli_query($db_connect, $sql)) {
            $res = mysqli_query($db_connect, $sql);
            $NUM = mysqli_num_rows($res);
            return $NUM;
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Admin Login Function Start--####***
    public function admin_login_check($data) {

        $db_connect = $this->__construct();
        $password = md5($data['txtpassword']);

        $sql = "SELECT * FROM tbl_user WHERE user_name ='$data[txtuserName]' AND password='$password' ";
        $query_result = mysqli_query($db_connect, $sql);

        if ($query_result) {
            $row = mysqli_fetch_assoc($query_result);
            if ($row) {
                $_SESSION['blood_user_id'] = $row['user_id'];
                $_SESSION['blood_user_name'] = $row['user_name'];
                $_SESSION['blood_full_name'] = $row['full_name'];
                $_SESSION['blood_user_type'] = $row['user_type'];
                $_SESSION['blood_profile_pic'] = $row['profile_pic'];
//                $_SESSION['care_access_permission'] = $row['access_permission'];
                header('Location: home.php');
            } else {
                $ex_message = "Please use valid User Name address and password";
                return $ex_message;
            }
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Admin Login Function End--####***
//****#####--Admin Logout Function Start--####***

    public function user_logout() {
        unset($_SESSION['blood_user_id']);
        unset($_SESSION['blood_user_name']);
        unset($_SESSION['blood_full_name']);
        unset($_SESSION['blood_user_type']);
        unset($_SESSION['blood_profile_pic']);
//        unset($_SESSION['care_access_permission']);
        header('Location: index.php');
        exit();
    }

//****#####--Admin Logout Function End--####***
//****#####--Save User Type Function Start--####***
    public function save_user_type_info($data) {
        $db_connect = $this->__construct();

        $user_type = $data[user_type];
        $query = mysqli_query($db_connect, "SELECT * FROM tbl_user_type WHERE user_type ='" . $user_type . "' AND deletion_status = 0 ");
        if (mysqli_num_rows($query) > 0) {
            $message = "<span style='color: white;background:#f44336;padding:8px;'>Sorry User Type is unique . This User Type is alrady exits !!</span>";
            return $message;
        } else {
            $sql = "INSERT INTO `tbl_user_type` (user_type,status) VALUES ('$data[user_type]','$data[status]')";

            if (mysqli_query($db_connect, $sql)) {
                $_SESSION['message'] = "User Type Info Save Successfully";
                header('Location: user_type.php');
                exit();
            } else {
                die('Query problem' . mysqli_error($db_connect));
            }
        }
    }

//****#####--Save User Type Function End--####***
//****#####--Select User Type Function Start--####***
    public function select_all_user_type_info() {

        $db_connect = $this->__construct();

        $sql = "SELECT * FROM `tbl_user_type` WHERE deletion_status= 0 ";
        $query_result = mysqli_query($db_connect, $sql);
        if ($query_result) {
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Select User Type Function End--####***
//****#####--Show User Type Function Start--####***
    public function show_user_type_info($id) {
        $db_connect = $this->__construct();

        $sql = "SELECT * FROM `tbl_user_type` WHERE id='$id' ";
        if (mysqli_query($db_connect, $sql)) {
            $result = mysqli_query($db_connect, $sql);
            return $result;
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Show User Type Function End--####***
//****#####--Delete User Type Function Start--####***
    public function delete_user_type($id) {
        $db_connect = $this->__construct();

        $sql = "DELETE FROM `tbl_user_type` WHERE id = '$id'";
        if (mysqli_query($db_connect, $sql)) {
            $message = "Delete Successfully";
            return $message;
        } else {
            die('Query Problem' . mysqli_error($db_connect));
        }
    }

//****#####--Delete User Type Function End--####***
//****#####--Update User Type Function Start--####***
    public function update_user_type_info($data) {
        $db_connect = $this->__construct();

        $sql = "UPDATE `tbl_user_type`
              SET   " . "user_type = '$data[user_type]',
                    " . "status = '$data[status]'" . "
              WHERE  id='$data[id]'";

        if (mysqli_query($db_connect, $sql)) {
            $_SESSION['message'] = " User Type Update successfully";
            header('Location: manage_user_type.php');
            exit();
        } else {
            die('Query Problem' . mysqli_error($db_connect));
        }
    }

//****#####--Update User Type Function End--####***
//****#####--Select Active User Type Function start--####***
    public function select_all_active_user_type() {
        $db_connect = $this->__construct();

        $sql = "SELECT * FROM `tbl_user_type` WHERE deletion_status= 0 AND status = 1 ";
        if (mysqli_query($db_connect, $sql)) {
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Select Active User Type Function End--####***
//****#####--Save User Function Start--####***
    public function save_user_info($data) {
        $db_connect = $this->__construct();

        $directory = 'profile_pic/';
        $target_file = $directory . $_FILES['profile_pic']['name'];
        $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
        $file_size = $_FILES['profile_pic']['size'];
        $check = getimagesize($_FILES['profile_pic']['tmp_name']);
        if ($check) {
            if (file_exists($target_file)) {
                $massage2 = "This file is already exists. please try new one";
                return $massage2;
            } else {
                if ($file_size > 10000000) {
                    $massage2 = "File is too large. please try new one";
                    return $massage2;
                } else {
                    if ($file_type != 'jpg' && $file_type != 'png') {
                        $massage2 = "File type is not valid. please try new one";
                        return $massage2;
                    } else {
                        move_uploaded_file($_FILES['profile_pic']['tmp_name'], $target_file);

                        $password = md5($data['txtPassword']);
                        $confirm_password = md5($data['txtConPass']);

                        if ($password == $confirm_password) {
                            $user_name = $data[txtUserName];
                            $txtCrDate = date('d-m-Y', strtotime($_POST['txtCrDate']));
                            $query = mysqli_query($db_connect, "SELECT * FROM tbl_user WHERE user_name ='" . $user_name . "' AND deletion_status = 0 ");
                            if (mysqli_num_rows($query) > 0) {
                                $message = "<span style='color: white;background:#f44336;padding:8px;'>Sorry User Name is unique . This User Name is alrady exits !!</span>";
                                return $message;
                            } else {
                                $sql = "INSERT INTO `tbl_user` (user_name,full_name,profile_pic,password,con_password,creation_date,user_type,status) VALUES ('$data[txtUserName]', '$data[txtfullName]', '$target_file','$password','$confirm_password','$txtCrDate','$data[txtUser_type]','$data[txtstatus]')";
                                if (mysqli_query($db_connect, $sql)) {
                                    $_SESSION['message'] = " Add User successfully";
                                    header('Location: user.php');
                                    exit();
                                }
                            }
                        } else {
                            $message = "<span style='color: white;background:#f44336;padding:8px;'>Sorry Password is Not Matching !!</span>";
                            return $message;
                        }
                    }
                }
            }
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Save User Function End--####***
//****#####--Update User Function start--####***
    public function update_profile_pic_info($data) {
        $db_connect = $this->__construct();

        $directory = 'profile_pic/';
        $target_file = $directory . $_FILES['profile_pic']['name'];
        $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
        $file_size = $_FILES['profile_pic']['size'];
        $check = getimagesize($_FILES['profile_pic']['tmp_name']);
        if ($check) {
            if (file_exists($target_file)) {
                $massage2 = "This file is already exists. please try new one";
                return $massage2;
            } else {
                if ($file_size > 10000000) {
                    $massage2 = "File is too large. please try new one";
                    return $massage2;
                } else {
                    if ($file_type != 'jpg' && $file_type != 'png') {
                        $massage2 = "File type is not valid. please try new one";
                        return $massage2;
                    } else {
                        move_uploaded_file($_FILES['profile_pic']['tmp_name'], $target_file);
                        $user_id = $data['user_id'];
                        $full_name = $data['full_name'];
                        $sql = "UPDATE `tbl_user`
                                    SET   " . "full_name = '$full_name',
                                     " . "profile_pic = '$target_file'" . "
                                     WHERE  user_id = '$user_id'";

                        if (mysqli_query($db_connect, $sql)) {
                            $message = "User  info save successfully";
                            return $message;
                        }
                    }
                }
            }
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Update User Function start--####***
//****#####--Update Password Function Start--####***
    public function update_password_info($data) {
        $db_connect = $this->__construct();
        $user_id = $data[user_id];
        $old = md5($data['oldPass']);
        $password = md5($data['newPassword']);
        $newpassword = md5($data['confirmnewPassword']);
        $query = mysqli_query($db_connect, "SELECT password FROM tbl_user WHERE user_id ='" . $user_id . "' AND deletion_status = 0 ");
        if ($query) {
            $row = mysqli_fetch_assoc($query);
            if ($row) {
                $oldpass2 = $row['password'];
                if ($oldpass2 == $old) {
                    if ($password == $newpassword) {
                        $sql = "UPDATE `tbl_user`
                        SET   " . "password = '$password',
                        " . "confirm_password = '$newpassword'" . "
                            WHERE  user_id = '$user_id'";

                        if (mysqli_query($db_connect, $sql)) {
                            $message = "<span style='color: white;background:#009933;padding:8px;'>Password Change Successfully  !!</span>";
                            return $message;
                        } else {
                            die('Query Problem' . mysqli_error($db_connect));
                        }
                    } else {
                        $message = "<span style='color: white;background:#f44336;padding:8px;'>Sorry confirm password is not matching !!</span>";
                        return $message;
                    }
                } else {
                    $message = "<span style='color: white;background:#f44336;padding:8px;'>Sorry your current password is not matching !!</span>";
                    return $message;
                }
            }
        } else {
            $message = "<span style='color: white;background:#f44336;padding:8px;'>Sorry This password is not this user !!</span>";
            return $message;
        }
    }

//****#####--Update Password Function End--####***
//****#####--Select User Function Start--####***
    public function select_all_user_info() {

        $db_connect = $this->__construct();

        $sql = "SELECT * FROM `tbl_user` WHERE deletion_status= 0 ";
        $query_result = mysqli_query($db_connect, $sql);
        if ($query_result) {
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Select User Function End--####***
//****#####--Delete User Function Start--####***
    public function delete_user_info($id) {

        $db_connect = $this->__construct();
        $sql = "DELETE FROM `tbl_user` WHERE user_id = '$id'";

        //$sql = "UPDATE `tbl_user` SET deletion_status=0 WHERE user_id = '$id' ";
        if (mysqli_query($db_connect, $sql)) {
            $message = "Delete Successfully";
            return $message;
        } else {
            die('Query Problem' . mysqli_error($db_connect));
        }
    }

//****#####--Delete User Function End--####***
//****#####--Show User  Function Start--####***
    public function show_user_info($id) {
        $db_connect = $this->__construct();

        $sql = "SELECT * FROM `tbl_user` WHERE user_id = '$id' ";
        if (mysqli_query($db_connect, $sql)) {
            $result = mysqli_query($db_connect, $sql);
            return $result;
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Show User  Function End--####***
////****#####--Update user  Function Start--####***
    public function update_user_info($data) {
        $db_connect = $this->__construct();

        $access_permission = $_POST['access_permission'];
        $chk1 = "";
        foreach ($access_permission as $chk2) {
            $chk1 .= $chk2 . ",";
        }
        $rest1 = substr($chk1, 0, strlen($chk1) - 1);

        $sql = "UPDATE `tbl_user` SET "
                . "user_name = '$data[user_name]',"
                . "full_name = '$data[full_name]',"
                . "branch_id = '$data[branch_id]',"
                . "user_type = '$data[user_type]',"
                . "status = '$data[status]',"
                . "access_permission = '$rest1'"
                . " WHERE user_id='$data[user_id]'";

        if (mysqli_query($db_connect, $sql)) {
            $_SESSION['message'] = " User Info successfully";
            header('Location: manage_user.php');
        } else {
            die('Query Problem' . mysqli_error($db_connect));
        }
    }

////****#####--Update user Function End--####***
//****#####--Save User Type Function Start--####***
    public function save_blood_group_info($data) {
        $db_connect = $this->__construct();

        $blood_group = $data[blood_group];
        $query = mysqli_query($db_connect, "SELECT * FROM tbl_blood_group WHERE blood_group ='" . $blood_group . "' AND deletion_status = 0 ");
        if (mysqli_num_rows($query) > 0) {
            $message = "<span style='color: white;background:#f44336;padding:8px;'>Sorry Blood Group is unique . This Blood Group is alrady exits !!</span>";
            return $message;
        } else {
            $sql = "INSERT INTO `tbl_blood_group` (blood_group,status) VALUES ('$data[blood_group]','$data[status]')";

            if (mysqli_query($db_connect, $sql)) {
                $_SESSION['message'] = "Blood Group Save Successfully";
                header('Location: add_group.php');
                exit();
            } else {
                die('Query problem' . mysqli_error($db_connect));
            }
        }
    }

//****#####--Save User Type Function End--####***
//****#####--Select User Type Function Start--####***
    public function select_all_blood_group_info() {

        $db_connect = $this->__construct();

        $sql = "SELECT * FROM `tbl_blood_group` WHERE deletion_status= 0 ";
        $query_result = mysqli_query($db_connect, $sql);
        if ($query_result) {
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Select User Type Function End--####***
//****#####--Delete User Type Function Start--####***
    public function delete_blood_group($id) {
        $db_connect = $this->__construct();

        $sql = "DELETE FROM `tbl_blood_group` WHERE id = '$id'";

        if (mysqli_query($db_connect, $sql)) {
            $_SESSION['message'] = "Blood Group Delete Successfully";
            header('Location: blood_group.php');
            exit();
        } else {
            die('Query Problem' . mysqli_error($db_connect));
        }
    }

//****#####--Delete User Type Function End--####***
//****#####--Save User Type Function Start--####***
    public function save_varsity_name_info($data) {
        $db_connect = $this->__construct();

        $varsity_name = $data[varsity_name];
        $query = mysqli_query($db_connect, "SELECT * FROM tbl_varsity_name WHERE varsity_name ='" . $varsity_name . "' AND deletion_status = 0 ");
        if (mysqli_num_rows($query) > 0) {
            $message = "<span style='color: white;background:#f44336;padding:8px;'>Sorry Varsity Name is unique . This Varsity Name is alrady exits !!</span>";
            return $message;
        } else {
            $sql = "INSERT INTO `tbl_varsity_name` (varsity_name,status) VALUES ('$data[varsity_name]','$data[status]')";

            if (mysqli_query($db_connect, $sql)) {
                $_SESSION['message'] = "Varsity Name Save Successfully";
                header('Location: add_varsity.php');
                exit();
            } else {
                die('Query problem' . mysqli_error($db_connect));
            }
        }
    }

//****#####--Save User Type Function End--####***
//****#####--Save User Type Function Start--####***
    public function save_city_name_info($data) {
        $db_connect = $this->__construct();

        $city_name = $data[city_name];
        $query = mysqli_query($db_connect, "SELECT * FROM tbl_city WHERE city_name ='" . $city_name . "' AND deletion_status = 0 ");
        if (mysqli_num_rows($query) > 0) {
            $message = "<span style='color: white;background:#f44336;padding:8px;'>Sorry City Name is unique . This Varsity Name is alrady exits !!</span>";
            return $message;
        } else {
            $sql = "INSERT INTO `tbl_city` (city_name,status) VALUES ('$data[city_name]','$data[status]')";

            if (mysqli_query($db_connect, $sql)) {
                $_SESSION['message'] = "City Name Save Successfully";
                header('Location: add_city.php');
                exit();
            } else {
                die('Query problem' . mysqli_error($db_connect));
            }
        }
    }

//****#####--Save User Type Function End--####***
//****#####--Save User Type Function Start--####***
    public function save_location_name_info($data) {
        $db_connect = $this->__construct();

        $city_id = $data[city_id];
        $location = $data[location];
        $query = mysqli_query($db_connect, "SELECT * FROM tbl_location WHERE city_id ='" . $city_id . "' AND location ='" . $location . "' AND deletion_status = 0 ");
        if (mysqli_num_rows($query) > 0) {
            $message = "<span style='color: white;background:#f44336;padding:8px;'>Sorry Location Name is unique . This Varsity Name is alrady exits !!</span>";
            return $message;
        } else {
            $sql = "INSERT INTO `tbl_location` (city_id,location) VALUES ('$data[city_id]','$data[location]')";

            if (mysqli_query($db_connect, $sql)) {
                $_SESSION['message'] = "Location Name Save Successfully";
                header('Location: location.php');
                exit();
            } else {
                die('Query problem' . mysqli_error($db_connect));
            }
        }
    }

//****#####--Save User Type Function End--####***
//****#####--Select User Type Function Start--####***
    public function select_all_city_info() {

        $db_connect = $this->__construct();

        $sql = "SELECT * FROM `tbl_city` WHERE deletion_status= 0 ";
        $query_result = mysqli_query($db_connect, $sql);
        if ($query_result) {
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Select User Type Function End--####***
//****#####--Select User Type Function Start--####***
    public function select_all_location_info() {

        $db_connect = $this->__construct();

        $sql = "SELECT * FROM `tbl_location` WHERE deletion_status= 0 ";
        $query_result = mysqli_query($db_connect, $sql);
        if ($query_result) {
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Select User Type Function End--####***
//****#####--Select User Type Function Start--####***
    public function select_all_varsity_info() {

        $db_connect = $this->__construct();

        $sql = "SELECT * FROM `tbl_varsity_name` WHERE deletion_status= 0 ";
        $query_result = mysqli_query($db_connect, $sql);
        if ($query_result) {
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Select User Type Function End--####***
//****#####--Save User Type Function Start--####***
    public function save_blood_seeker_content($data) {
        $db_connect = $this->__construct();

        if ($_FILES['donorPic']['name'] != '') {
            $tmp_name = $_FILES["donorPic"]["tmp_name"];
            $namefile = $_FILES["donorPic"]["name"];
            $ext = end(explode(".", $namefile));
            $donorPic = time() . "." . $ext;
            $fileUpload = move_uploaded_file($_FILES['donorPic']['tmp_name'], "donorPic/" . $donorPic);
        }
        $dob = date('d-m-Y', strtotime($_POST['dob']));
        $reg_date = date('d-m-Y', strtotime($_POST['registration_date']));
        $sql = "INSERT INTO `tbl_blood_seeker` "
                . "(seeker_name,"
                . "father_name,"
                . "mother_name,"
                . "current_address,"
                . "permanent_address,"
                . "dob,"
                . "email_address,"
                . "nID,"
                . "phone_number,"
                . "current_job_location,"
                . "mobile_number,"
                . "certificated_from,"
                . "donorPic,"
                . "blood_group,"
                . "registration_date,"
                . "status)"
                . " VALUES "
                . "('$data[seeker_name]',"
                . "'$data[father_name]',"
                . "'$data[mother_name]',"
                . "'$data[current_address]',"
                . "'$data[permanent_address]',"
                . "'$dob',"
                . "'$data[email_address]',"
                . "'$data[nID]',"
                . "'$data[phone_number]',"
                . "'$data[current_job_location]',"
                . "'$data[mobile_number]',"
                . "'$data[certificated_from]',"
                . "'$donorPic',"
                . "'$data[blood_group]',"
                . "'$reg_date',"
                . "'$data[status]')";

        if (mysqli_query($db_connect, $sql)) {
            $_SESSION['message'] = "Blood Seeker Save Successfully";
            header('Location: blood_seeker_reg.php');
            exit();
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Save User Type Function End--####***
//****#####--Save User Type Function Start--####***
    public function save_blood_donor_content($data) {
        $db_connect = $this->__construct();

        if ($_FILES['donorImages']['name'] != '') {
            $tmp_name = $_FILES["donorImages"]["tmp_name"];
            $namefile = $_FILES["donorImages"]["name"];
            $ext = end(explode(".", $namefile));
            $donorImages = time() . "." . $ext;
            $fileUpload = move_uploaded_file($_FILES['donorImages']['tmp_name'], "donorImages/" . $donorImages);
        }
        $dob = date('d-m-Y', strtotime($_POST['dob']));
        $reg_date = date('d-m-Y', strtotime($_POST['registration_date']));
        $sql = "INSERT INTO `tbl_blood_donor` "
                . "(donor_name,"
                . "father_name,"
                . "mother_name,"
                . "current_address,"
                . "permanent_address,"
                . "dob,"
                . "email_address,"
                . "city_id,"
                . "nID,"
                . "phone_number,"
                . "varsity_name,"
                . "mobile_number,"
                . "alternative_number,"
                . "donorImages,"
                . "blood_group,"
                . "registration_date,"
                . "location_id,"
                . "status)"
                . " VALUES "
                . "('$data[donor_name]',"
                . "'$data[father_name]',"
                . "'$data[mother_name]',"
                . "'$data[current_address]',"
                . "'$data[permanent_address]',"
                . "'$dob',"
                . "'$data[email_address]',"
                . "'$data[city_id]',"
                . "'$data[nID]',"
                . "'$data[phone_number]',"
                . "'$data[varsity_name]',"
                . "'$data[mobile_number]',"
                . "'$data[alternative_number]',"
                . "'$donorImages',"
                . "'$data[blood_group]',"
                . "'$reg_date',"
                . "'$data[location_id]',"
                . "'$data[status]')";

        if (mysqli_query($db_connect, $sql)) {
            $_SESSION['message'] = "Blood Donor Save Successfully";
            header('Location: blood_donor_reg.php');
            exit();
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Save User Type Function End--####***
//****#####--Save User Type Function Start--####***
    public function save_organization_info($data) {
        $db_connect = $this->__construct();

        $organization_name = $data[organization_name];
        $query = mysqli_query($db_connect, "SELECT * FROM tbl_organization WHERE organization_name ='" . $organization_name . "' AND deletion_status = 0 ");
        if (mysqli_num_rows($query) > 0) {
            $message = "<span style='color: white;background:#f44336;padding:8px;'>Sorry Varsity Name is unique . This Varsity Name is alrady exits !!</span>";
            return $message;
        } else {
            $sql = "INSERT INTO `tbl_organization` (organization_name,websiteLink) VALUES ('$data[organization_name]','$data[websiteLink]')";

            if (mysqli_query($db_connect, $sql)) {
                $_SESSION['message'] = "Organization Name Save Successfully";
                header('Location: add_varsity.php');
                exit();
            } else {
                die('Query problem' . mysqli_error($db_connect));
            }
        }
    }

//****#####--Save User Type Function End--####***
//****#####--Select Active User Type Function start--####***
    public function select_all_active_blood_donor() {
        $db_connect = $this->__construct();

        $sql = "SELECT * FROM `tbl_blood_donor` WHERE deletion_status= 0 AND status = 1 ";
        if (mysqli_query($db_connect, $sql)) {
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Select Active User Type Function End--####***
//****#####--Select Active User Type Function start--####***
    public function select_all_service_organization() {
        $db_connect = $this->__construct();

        $sql = "SELECT * FROM `tbl_organization` WHERE deletion_status= 0";
        if (mysqli_query($db_connect, $sql)) {
            $query_result = mysqli_query($db_connect, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Select Active User Type Function End--####***
//****#####--Save User Type Function Start--####***
    public function save_blood_collection_content($data) {
        $db_connect = $this->__construct();

        $donation_date = date('d-m-Y', strtotime($_POST['donation_date']));
        $sql = "INSERT INTO `tbl_blood_collection` "
                . "(donor_id,"
                . "blood_group,"
                . "blood_amount,"
                . "donation_date,"
                . "donation_time,"
                . "organization_id)"
                . " VALUES "
                . "('$data[donor_id]',"
                . "'$data[blood_group]',"
                . "'$data[blood_amount]',"
                . "'$donation_date',"
                . "'$data[donation_time]',"
                . "'$data[organization_id]')";

        if (mysqli_query($db_connect, $sql)) {
            $_SESSION['message'] = "Blood Collection Save Successfully";
            header('Location: blood_collection.php');
            exit();
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Save User Type Function End--####***
//****#####--Select User Type Function Start--####***
    public function select_all_appoinment_request() {

        $db_connect = $this->__construct();

        $sql = "SELECT * FROM `tbl_appoinment_request` WHERE deletion_status= 0 ";
        $query_result = mysqli_query($db_connect, $sql);
        if ($query_result) {
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Select User Type Function End--####***
//****#####--Select User Type Function Start--####***
    public function select_all_blood_request_info() {

        $db_connect = $this->__construct();

        $sql = "SELECT * FROM `tbl_blood_request` WHERE deletion_status= 0 ";
        $query_result = mysqli_query($db_connect, $sql);
        if ($query_result) {
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Select User Type Function End--####***
//****#####--Save User Type Function Start--####***
    public function save_blood_donation_process($data) {
        $db_connect = $this->__construct();

        if ($_FILES['donationPic']['name'] != '') {
            $tmp_name = $_FILES["donationPic"]["tmp_name"];
            $namefile = $_FILES["donationPic"]["name"];
            $ext = end(explode(".", $namefile));
            $donationPic = time() . "." . $ext;
            $fileUpload = move_uploaded_file($_FILES['donationPic']['tmp_name'], "donationPic/" . $donationPic);
        }
        $dob = date('d-m-Y', strtotime($_POST['dob']));
        $reg_date = date('d-m-Y', strtotime($_POST['registration_date']));
        $sql = "INSERT INTO `tbl_donation_process` "
                . "(title,"
                . "donationPic,"
                . "content)"
                . " VALUES "
                . "('$data[title]',"
                . "'$donationPic',"
                . "'$data[content]')";

        if (mysqli_query($db_connect, $sql)) {
            $_SESSION['message'] = "Save Successfully";
            header('Location: donation_process.php');
            exit();
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Save User Type Function End--####***
//****#####--Save User Type Function Start--####***
    public function save_gallery_content($data) {
        $db_connect = $this->__construct();

        if ($_FILES['gallery_image']['name'] != '') {
            $tmp_name = $_FILES["gallery_image"]["tmp_name"];
            $namefile = $_FILES["gallery_image"]["name"];
            $ext = end(explode(".", $namefile));
            $gallery_image = time() . "." . $ext;
            $fileUpload = move_uploaded_file($_FILES['gallery_image']['tmp_name'], "gallery_image/" . $gallery_image);
        }
        $sql = "INSERT INTO `tbl_gallery` "
                . "(gallery_image,"
                . "status)"
                . " VALUES "
                . "('$gallery_image',"
                . "'$data[status]')";

        if (mysqli_query($db_connect, $sql)) {
            $_SESSION['message'] = "Save Successfully";
            header('Location: gallery.php');
            exit();
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }
//****#####--Save User Type Function End--####***
//****#####--Save User Type Function Start--####***
    public function save_sponsor_content($data) {
        $db_connect = $this->__construct();

        if ($_FILES['sponsor_image']['name'] != '') {
            $tmp_name = $_FILES["sponsor_image"]["tmp_name"];
            $namefile = $_FILES["sponsor_image"]["name"];
            $ext = end(explode(".", $namefile));
            $sponsor_image = time() . "." . $ext;
            $fileUpload = move_uploaded_file($_FILES['sponsor_image']['tmp_name'], "sponsor_image/" . $sponsor_image);
        }
        $sql = "INSERT INTO `tbl_sponsor` "
                . "(sponsor_image,"
                . "status)"
                . " VALUES "
                . "('$sponsor_image',"
                . "'$data[status]')";

        if (mysqli_query($db_connect, $sql)) {
            $_SESSION['message'] = "Save Successfully";
            header('Location: sponsor.php');
            exit();
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Save User Type Function End--####***
//****#####--Save User Type Function Start--####***
    public function save_campaign_content($data) {
        $db_connect = $this->__construct();

        if ($_FILES['campaign_image']['name'] != '') {
            $tmp_name = $_FILES["campaign_image"]["tmp_name"];
            $namefile = $_FILES["campaign_image"]["name"];
            $ext = end(explode(".", $namefile));
            $campaign_image = time() . "." . $ext;
            $fileUpload = move_uploaded_file($_FILES['campaign_image']['tmp_name'], "campaign_image/" . $campaign_image);
        }

        if ($_FILES['creator_images']['name'] != '') {
            $tmp_name = $_FILES["creator_images"]["tmp_name"];
            $namefile = $_FILES["creator_images"]["name"];
            $ext = end(explode(".", $namefile));
            $creator_images = time() . "." . $ext;
            $fileUpload = move_uploaded_file($_FILES['creator_images']['tmp_name'], "creator_images/" . $creator_images);
        }

        $even_date = date('d-m-Y', strtotime($_POST['even_date']));

        $sql = "INSERT INTO `tbl_campaign` "
                . "(even_name,"
                . "even_title,"
                . "even_venue,"
                . "campaign_image,"
                . "even_date,"
                . "even_time,"
                . "even_creator,"
                . "creator_images,"
                . "creator_opinion,"
                . "even_description)"
                . " VALUES "
                . "('$data[even_name]',"
                . "'$data[even_title]',"
                . "'$data[even_venue]',"
                . "'$campaign_image',"
                . "'$even_date',"
                . "'$data[even_time]',"
                . "'$data[even_creator]',"
                . "'$creator_images',"
                . "'$data[creator_opinion]',"
                . "'$data[even_description]')";

        if (mysqli_query($db_connect, $sql)) {
            $_SESSION['message'] = "Save Successfully";
            header('Location: campaign.php');
            exit();
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

//****#####--Save User Type Function End--####***
//****#####--Save User Type Function Start--####***
    public function save_slaider_content($data) {
        $db_connect = $this->__construct();

        if ($_FILES['slider_image']['name'] != '') {
            $tmp_name = $_FILES["slider_image"]["tmp_name"];
            $namefile = $_FILES["slider_image"]["name"];
            $ext = end(explode(".", $namefile));
            $slider_image = time() . "." . $ext;
            $fileUpload = move_uploaded_file($_FILES['slider_image']['tmp_name'], "slider_image/" . $slider_image);
        }
        $sql = "INSERT INTO `tbl_slider` "
                . "(slider_image,"
                . "status)"
                . " VALUES "
                . "('$slider_image',"
                . "'$data[status]')";

        if (mysqli_query($db_connect, $sql)) {
            $_SESSION['message'] = "Save Successfully";
            header('Location: slider.php');
            exit();
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }

    //****#####--Save User Type Function End--####***
    //****#####--Save User Type Function Start--####***
    public function save_volunteer_content($data) {
        $db_connect = $this->__construct();

        if ($_FILES['slider_image']['name'] != '') {
            $tmp_name = $_FILES["slider_image"]["tmp_name"];
            $namefile = $_FILES["slider_image"]["name"];
            $ext = end(explode(".", $namefile));
            $slider_image = time() . "." . $ext;
            $fileUpload = move_uploaded_file($_FILES['slider_image']['tmp_name'], "slider_image/" . $slider_image);
        }
        $sql = "INSERT INTO `tbl_volunteer` "
                . "(slider_image,"
                . "status)"
                . " VALUES "
                . "('$slider_image',"
                . "'$data[status]')";

        if (mysqli_query($db_connect, $sql)) {
            $_SESSION['message'] = "Save Successfully";
            header('Location: slider.php');
            exit();
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }
//****#####--Save User Type Function End--####***
//****#####--Save User Type Function Start--####***
    public function save_user_opinion_content($data) {
        $db_connect = $this->__construct();

        $content = $data[user_opinion];
        $infoContent = htmlentities($content);
        $entity_elm1 = mysqli_real_escape_string($db_connect, $infoContent);

        $sql = "INSERT INTO `tbl_user_opinion` "
                . "(opinion_title,"
                . "user_opinion,"
                . "user_name,"
                . "user_address)"
                . " VALUES "
                . "('$data[opinion_title]',"
                . "'$entity_elm1',"
                . "'$data[user_name]',"
                . "'$data[user_address]')";

        if (mysqli_query($db_connect, $sql)) {
            $_SESSION['message'] = "Save Successfully";
            header('Location: donor_opinion.php');
            exit();
        } else {
            die('Query problem' . mysqli_error($db_connect));
        }
    }
//****#####--Save User Type Function End--####***
}
