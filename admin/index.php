<?php
require './Admin.php';
error_reporting(E_ALL);
$obj_user = new Admin();
session_start();
if (isset($_SESSION['blood_user_id'])) {
    $user_id = $_SESSION['blood_user_id'];
    if ($user_id != NULL) {
        header('Location: home.php');
    }
}
if (isset($_POST['btn'])) {
    $message = $obj_user->admin_login_check($_POST);
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="/plugins/images/favicon.png">
        <title>Blood Guardianship</title>
        <!-- Bootstrap Core CSS -->
        <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="css/animate.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/style.css" rel="stylesheet">
        <!-- color CSS -->
        <link href="css/colors/default-dark.css" id="theme" rel="stylesheet">
    </head>

    <body>
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <section id="wrapper" class="login-register">
            <div class="login-box">
                <div class="white-box">
                    <form method="POST" action="" class="form-horizontal form-material" id="loginform">
                        <div align="center"><img class="img-circle" src="plugins/images/logo2.jpg" style="margin-bottom: 20px;" width="100" height="100"></div>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input name="txtuserName" class="form-control" style="color: #fff" type="text" required="" placeholder="Username">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input name="txtpassword" class="form-control" style="color: #fff" type="password" required="" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button type="submit" name="btn" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light">Log In</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- jQuery -->
        <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="bootstrap/dist/js/tether.min.js"></script>
        <script src="bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <!--slimscroll JavaScript -->
        <script src="js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="js/waves.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="js/custom.min.js"></script>
        <!--Style Switcher -->
        <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    </body>


    <!-- Mirrored from eliteadmin.themedesigner.in/demos/eliteadmin-dark/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 16 Dec 2017 20:08:23 GMT -->
</html>
