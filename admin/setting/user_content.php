<?php
$query_result = $obj_user->select_all_active_user_type();
if (isset($_POST['btn'])) {
    $message = $obj_user->save_user_info($_POST);
}
?>
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">User Content</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="home.php">Dashboard</a></li>
                <li><a href="./manage_user.php">View User</a></li>
                <li class="active">Add User</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!--.row-->
    <div class="row">
        <div class="col-md-12">
            <?php
            if (isset($_SESSION['message'])) {
                ?>          
                <div class="alert alert-success">
                    <a href="#" class="alert-link" style="text-align: center;"><?php echo $_SESSION['message']; ?></a>.
                </div>
                <?php
                unset($_SESSION['message']);
            }
            ?>
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <form method="POST" action="" enctype="multipart/form-data">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputuname">User Name</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="txtUserName" class="form-control" placeholder="Username"> </div>
                                </div> 
                                <div class="form-group">
                                    <label for="exampleInputuname">Full Name</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="txtfullName" class="form-control" placeholder="Full Name"> </div>
                                </div> 
                                <div class="form-group">
                                    <label for="exampleInputpwd1">Password</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                                        <input type="password" name="txtPassword" class="form-control" id="exampleInputpwd1" placeholder="Enter Password"> </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputpwd1">Creation Date</label>
                                    <div class="input-group">
                                        <input type="text" name="txtCrDate" class="form-control" id="datepicker-autoclose" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputuname">Profile Picture</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="file" name="profile_pic" class="form-control"  placeholder="Profile Picture"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputpwd2">Confirm Password</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                                        <input type="password" name="txtConPass" class="form-control" id="exampleInputpwd2" placeholder="Confirm Password"> </div>
                                </div>
                                <div class="form-group">
                                    <label>User Type</label>
                                    <select name="txtUser_type" class="form-control select2" style="color: #fff">
                                        <option>Select User Type</option>
                                        <?php foreach ($query_result as $user_type) { ?>
                                            <option value="<?php echo $user_type['id']; ?>"><?php echo $user_type['user_type']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="txtstatus" class="form-control select2" style="color: #fff">
                                        <option>Select Status</option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div align="center">
                                <button type="submit" name="btn" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>         
    </div>
</div>