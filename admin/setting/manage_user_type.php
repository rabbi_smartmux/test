<?php
if (isset($_GET['name'])) {
    $id = $_GET['id'];

    $message = $obj_user->delete_user_type($id);
}
$query_result = $obj_user->select_all_user_type_info();
?>
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">User Type</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="home.php">Dashboard</a></li>
                <li><a href="./user_type.php">Add User Type</a></li>
                <li class="active">View User Type</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table id="myTable" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">SL</th>
                                <th class="text-center">User TYPE</th>
                                <th class="text-center">STATUS</th>
                                <th class="text-center">ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($query_result as $value) {
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo $i; ?></td>
                                    <td class="text-center"><?php echo $value['user_type']; ?></td>
                                    <td class="text-center">
                                        <?php
                                        if ($value['status'] == 1) {
                                            ?>
                                            <div class="label label-table label-success">Active</div>
                                            <?php
                                        } else {
                                            ?>
                                            <div class="label label-table label-danger">Inactive</div>
                                        <?php } ?>
                                    </td>
                                    <td class="text-nowrap text-center">
                                        <a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                        <a href="?name=delete&id=<?php echo $value['id']; ?>" onclick=" return checkDelete();" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>