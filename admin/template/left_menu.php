<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <div class="user-profile">
            <div class="dropdown user-pro-body">
                <div><img src="<?php echo $_SESSION['blood_profile_pic']; ?>" alt="user-img" class="img-circle"></div> <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['blood_full_name']; ?><span class="caret"></span></a>
                <ul class="dropdown-menu animated flipInY">
                    <li><a href="#"><i class="ti-user"></i><?php echo ' ' . $_SESSION['blood_full_name']; ?></a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="?status=logout"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
            </div>
        </div>
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
                        <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                    </span> </div>
                <!-- /input-group -->
            </li>
            <li> <a href="home.php" class="waves-effect"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i> <span class="hide-menu">Dashboard</span></a> </li>
            <li> <a href="forms.html" class="waves-effect"><i data-icon="P" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Admin<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="./manage_user_type.php">User Type</a></li>
                    <li><a href="./manage_user.php">User</a></li>
                </ul>
            </li>
            <li> <a href="forms.html" class="waves-effect"><i data-icon="P" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Information<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="./blood_group.php">Blood Group</a></li>
                    <li><a href="./view_varsity.php">Varsity Name</a></li>
                    <li><a href="./service_organization.php">Service Organization</a></li>
                    <li><a href="./add_city.php">Add City</a></li>
                    <li><a href="./location.php">Add Location</a></li>
                </ul>
            </li>
            <li> <a href="forms.html" class="waves-effect"><i class="fa fa-male"></i><span class="hide-menu"> Registration<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="./blood_seeker_reg.php">Seeker Registration</a></li>
                    <li><a href="./blood_donor_reg.php">Donor Registration</a></li>
                    <li><a href="./blood_collection.php">Blood Collection</a></li>
                </ul>
            </li>
            <li> <a href="forms.html" class="waves-effect"><i class="fa fa-male"></i><span class="hide-menu"> User Request<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="./appoinment_request.php">Appointment Request</a></li>
                    <li><a href="./blood_request.php">Blood Request</a></li>
                </ul>
            </li>
            <li> <a href="forms.html" class="waves-effect"><i class="fa fa-bar-chart"></i><span class="hide-menu"> Website Setting<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="./donation_process.php">Donation Process</a></li>
                    <li><a href="./blood_request.php">Blood Request</a></li>
                    <li><a href="./slider.php">Slider</a></li>
                    <li><a href="./gallery.php">Gallery</a></li>
                    <li><a href="./sponsor.php">Sponsor</a></li>
                    <li><a href="./campaign.php">Campaign</a></li>
                    <li><a href="./donor_opinion.php">Opinion</a></li>
                    <li><a href="./add_volunteers.php">Volunteers</a></li>
                 </ul>
            </li>
        </ul>
    </div>
</div>
