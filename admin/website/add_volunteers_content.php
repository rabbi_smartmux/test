<?php
if (isset($_POST['btn'])) {
    $message = $obj_user->save_volunteer_content($_POST);
}
?>
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Slider Content</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="home.php">Dashboard</a></li>
                <li><a href="./blood_group.php">View Volunteer Content</a></li>
                <li class="active">Volunteer Content</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!--.row-->
    <div class="row">
        <div class="col-md-12">
            <?php
            if (isset($_SESSION['message'])) {
                ?>
                <div class="alert alert-success">
                    <a href="#" class="alert-link" style="text-align: center;"><?php echo $_SESSION['message'];?></a>.
                </div>
                <?php
                 unset($_SESSION['message']);
            }
            ?>
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <form method="POST" action="" enctype="multipart/form-data">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputuname">Volunteer Name</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-email"></i></div>
                                        <input type="text" name="volunteerName" class="form-control" placeholder = "Volunteer Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputuname">Volunteer Image</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-email"></i></div>
                                        <input type="file" name="volunteerImage" class="form-control" title="Volunteer Image">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputuname">Facebook Link</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-email"></i></div>
                                        <input type="url" name="facebookLink" class="form-control" placeholder = "Facebook Link">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Designation</label>
                                    <input type="text" name="designation" class="form-control" placeholder = "Designation">
                                    <!--<textarea  name="content" cols="1" rows="1" class="form-control" placeholder = "Designation"></textarea>-->
                                </div>
                            </div>
                            <div align="center">
                                <button type="submit" name="btn" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
