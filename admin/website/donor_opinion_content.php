<?php
if (isset($_POST['btn'])) {
    $message = $obj_user->save_user_opinion_content($_POST);
}
?>
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Donor Opinion</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="home.php">Dashboard</a></li>
                <li><a href="./manage_user.php">View Donor Opinion</a></li>
                <li class="active">Add Donor Opinion</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!--.row-->
    <div class="row">
        <div class="col-md-12">
            <?php
            if (isset($_SESSION['message'])) {
                ?>          
                <div class="alert alert-success">
                    <a href="#" class="alert-link" style="text-align: center;"><?php echo $_SESSION['message']; ?></a>.
                </div>
                <?php
                unset($_SESSION['message']);
            }
            ?>
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <form method="POST" action="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputuname">Opinion Title</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="opinion_title" class="form-control" placeholder="Opinion Title" required=""> 
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label for="exampleInputuname">Opinion</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-email"></i></div>
                                        <input type="text" name="user_opinion" class="form-control" placeholder="Opinion" required=""> 
                                    </div>
                                </div>         
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputuname">User Name</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="user_name" class="form-control" placeholder="User Name" required=""> 
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label for="exampleInputuname">User Address</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-email"></i></div>
                                        <input type="text" name="user_address" class="form-control" placeholder="User Address" required=""> 
                                    </div>
                                </div> 
                            </div>
                            <div align="center">
                                <button type="submit" name="btn" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>         
    </div>
</div>