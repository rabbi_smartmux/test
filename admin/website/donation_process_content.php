<?php
$query_result = $obj_user->select_all_active_user_type();
if (isset($_POST['btn'])) {
    $message = $obj_user->save_blood_donation_process($_POST);
}
?>
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Donation Process</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="home.php">Dashboard</a></li>
                <li><a href="./manage_user.php">View Donation Process</a></li>
                <li class="active">Add Donation Process</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!--.row-->
    <div class="row">
        <div class="col-md-12">
            <?php
            if (isset($_SESSION['message'])) {
                ?>          
                <div class="alert alert-success">
                    <a href="#" class="alert-link" style="text-align: center;"><?php echo $_SESSION['message']; ?></a>.
                </div>
                <?php
                unset($_SESSION['message']);
            }
            ?>
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <form method="POST" action="" enctype="multipart/form-data">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputuname">Title</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="title" class="form-control" placeholder="Title" required=""> 
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label for="exampleInputuname">Donation Picture</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="file" name="donationPic" class="form-control" required=""> 
                                    </div>
                                </div>


                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputuname">Content</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-email"></i></div>
                                        <textarea name="content" cols="1" rows="7" class="form-control" placeholder="Content" required=""></textarea>
                                    </div>
                                </div> 
                            </div>
                            <div align="center">
                                <button type="submit" name="btn" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>         
    </div>
</div>