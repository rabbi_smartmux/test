<?php
if (isset($_POST['btn'])) {
    $message = $obj_user->save_campaign_content($_POST);
}
?>
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Campaign</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="home.php">Dashboard</a></li>
                <li><a href="./manage_user.php">View Campaign</a></li>
                <li class="active">Add Campaign</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!--.row-->
    <div class="row">
        <div class="col-md-12">
            <span style="font-size: 14px;color: #fff">
                All star marked ( <span style="color:red;">*</span> ) fields are mandatory, please fill up all mandatory fields.
            </span>
            <?php
            if (isset($_SESSION['message'])) {
                ?>          
                <div class="alert alert-success">
                    <a href="#" class="alert-link" style="text-align: center;"><?php echo $_SESSION['message']; ?></a>.
                </div>
                <?php
                unset($_SESSION['message']);
            }
            ?>
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <form method="POST" action="" enctype="multipart/form-data">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputuname">Even Name<span style="color: red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="even_name" class="form-control" placeholder="Even Title"> 
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label for="exampleInputuname">Even Title<span style="color: red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="even_title" class="form-control" placeholder="Even Name"> 
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label for="exampleInputuname">Even Venue<span style="color: red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="even_venue" class="form-control" placeholder="Even Venue"> 
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label for="exampleInputuname">Campaign Image</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="file" name="campaign_image" class="form-control" title="Campaign Image"> 
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top: -10px;">
                                    <label for="exampleInputpwd1">Even Date<span style="color: red">*</span></label>
                                    <div class="input-group">
                                        <input type="text" name="even_date" class="form-control" id="datepicker-autoclose" placeholder="mm/dd/yyyy"> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputpwd1">Even Time<span style="color: red">*</span></label>
                                    <div class="input-group clockpicker">
                                        <input type="text" name="even_time" class="form-control" value="09:30"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputuname">Even Creator</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="even_creator" class="form-control" placeholder="Even Creator"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputuname">Creator Images<span style="color: red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="file" name="creator_images" class="form-control" placeholder="Creator Images"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputuname">Creator Opinion</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <textarea name="creator_opinion" cols="1" rows="1" class="form-control" placeholder="Creator Opinion"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputuname">Even Description<span style="color: red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <textarea name="even_description" cols="1" rows="1" class="form-control" placeholder="Even Description"></textarea> 
                                    </div>
                                </div>
                            </div>
                            <div align="center">
                                <button type="submit" name="btn" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>         
    </div>
</div>