<?php
$city = $obj_user->select_all_city_info();
if (isset($_POST['btn'])) {
    $message = $obj_user->save_location_name_info($_POST);
}
?>
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Add Location</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="home.php">Dashboard</a></li>
                <li><a href="#">View Location</a></li>
                <li class="active">Add Location</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!--.row-->
    <div class="row">
        <div class="col-md-12">
            <?php
            if (isset($_SESSION['message'])) {
                ?>          
                <div class="alert alert-success">
                    <a href="#" class="alert-link" style="text-align: center;"><?php echo $_SESSION['message']; ?></a>.
                </div>
                <?php
                unset($_SESSION['message']);
            }
            ?>
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <form method="POST" action="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>City Name</label>
                                    <select name="city_id" class="form-control select2" style="color: #fff">
                                        <option>Select City Name</option>
                                        <?php foreach ($city as $value) { ?>
                                        <option value="<?php echo $value['id'];?>"><?php echo $value['city_name'];?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputuname">Location Name</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-user"></i></div>
                                        <input type="text" name="location" class="form-control" placeholder="Location Name"> </div>
                                </div> 
                            </div>

                            <div align="center">
                                <button type="submit" name="btn" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>         
    </div>
</div>