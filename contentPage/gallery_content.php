<?php
$gallery = $ob_app->select_all_gallery_image_info();
?>
<section class="page-header" data-stellar-background-ratio="1.2">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h3>
                    Gallery
                </h3>
            </div>
        </div> <!-- end .row  -->
    </div> <!-- end .container  -->
</section> <!-- end .page-header  -->
<!--  GALLERY CONTENT  -->

<section class="section-content-block section-secondary-bg">

    <div class="container">

        <div class="row section-heading-wrapper">

            <div class="col-md-12 col-sm-12 text-center">
                <h2 class="section-heading"><span>Photo</span> Gallery</h2>
                <p class="section-subheading">
                    Campaign photos of different parts of world and our prestigious voluntary work
                </p>
            </div> <!-- end .col-sm-10  -->

        </div> <!-- end .row  -->

    </div> <!--  end .container -->

    <div class="container wow fadeInUp">
        <div class="no-padding-gallery gallery-carousel">
            <?php foreach ($gallery as $value) { ?>
            <a class="gallery-light-box xs-margin" data-gall="myGallery" href="admin/gallery_image/<?php echo $value['gallery_image'];?>">
                <figure class="gallery-img">
                    <img src="admin/gallery_image/<?php echo $value['gallery_image'];?>" alt="gallery image" />
                </figure> <!-- end .cause-img  -->
            </a> <!-- end .gallery-light-box  -->
            <?php }?>
         </div> <!-- end .row  -->
    </div><!-- end .container-fluid  -->

</section> <!-- end .section-content-block  -->

<!-- START FOOTER  -->