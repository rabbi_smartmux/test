<?php
$blood_group = $ob_app->select_all_blood_group_info();
$varsity = $ob_app->select_all_varsity_info();
$city = $ob_app->select_all_city_info();
$location = $ob_app->select_all_location_info();
?>
<section class="page-header" data-stellar-background-ratio="1.2">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h3>
                    Find Blood Donor
                </h3>
            </div>
        </div> <!-- end .row  -->
    </div> <!-- end .container  -->
</section> <!-- end .page-header  -->
<!--  MAIN CONTENT  -->
<section class="section-content-block section-contact-block no-bottom-padding">
    <div class="container">
        <div class="row">
            <div class ="col-md-12">
                <div class="row section-heading-wrapper">

                    <div class="col-md-12 col-sm-12 text-center">
                        <h2 class="section-heading">Find A Blood Donor</h2>
                    </div> <!-- end .col-sm-10  -->                      
                </div>                          
            </div>               
            <div class="col-md-12">
                <div style="border: 1px solid;display:block;overflow:auto;padding: 20px;margin-bottom: 40px">
                    <form method="POST" action="">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Blood Group</label>
                                <select name="blood_group" id="inputState" class="form-control">
                                    <option selected>Please Select Blood Group</option>
                                    <?php foreach ($blood_group as $group) { ?>
                                        <option value="<?php echo $group['blood_group']; ?>"><?php echo $group['blood_group']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Varsity Name</label>
                                <select name="varsity_id" id="inputState" class="form-control">
                                    <option selected>Please Select Varsity Name</option>
                                    <?php foreach ($varsity as $varsityName) { ?>
                                        <option value="<?php echo $varsityName['id']; ?>"><?php echo $varsityName['varsity_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">City</label>
                                <select name="location_id" id="inputState" class="form-control">
                                    <option selected>Please Select County</option>
                                    <?php foreach ($location as $value) { ?>
                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['location']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Location</label>
                                <select name="city_id" id="inputState" class="form-control">
                                    <option selected>Please Select Location</option>
                                    <?php foreach ($city as $value) { ?>
                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['city_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                        </div>

                        <div class="form-group col-md-12 col-sm-12 col-xs-12" align="center">
                            <button name="search_text" id="btn_submit" class="btn btn-success" type="submit">Find Donor</button>
                        </div>
                    </form>
                </div>
                <?php
                if (isset($_POST['search_text'])) {

                    $blood_group = $_POST['blood_group'];
                    $varsity_id = $_POST['varsity_id'];
                    $location_id = $_POST['location_id'];
                    $city_id = $_POST['city_id'];
                    $query_result = $ob_app->select_all_donor_info($city_id, $varsity_id, $blood_group, $location_id);
                    ?>
                    <table style="margin-top: 20px" id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr style="background-color: #3399ff;color: #fff">
                                <th>SL</th>
                                <th>Donar Name</th>
                                 <th>Mobile No</th>
                                <th>Email Address</th>
                                <th>Available/Unavailable</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($query_result as $donorInfo) {
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $donorInfo['donor_name']; ?></td>
                                    <td><?php echo $donorInfo['mobile_number']; ?></td>
                                    <td><?php echo $donorInfo['email_address']; ?></td>
                                    <td class="text-center">
                                        <?php
                                        if ($donorInfo['status'] == 1) {
                                            ?>
                                            <div class="label label-table label-success">Available</div>
                                            <?php
                                        } else {
                                            ?>
                                            <div class="label label-table label-danger">Unavailable</div>
                                        <?php } ?>
                                    </td>
                                    <td class="text-nowrap text-center">
                                        <a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-eye text-inverse m-r-10"></i> </a>
                                     </td>
                                </tr>
                                <?php $i++;
                            }
                            ?>
                        </tbody>
                    </table>
<?php } ?>
            </div>
        </div> 
    </div>
</section>
