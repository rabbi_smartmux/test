<?php
$campaign = $ob_app->select_all_campaign_info();
?>
<section class="page-header" data-stellar-background-ratio="1.2">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h3>
                    Campaign Lists
                </h3>
            </div>
        </div> <!-- end .row  -->
    </div> <!-- end .container  -->
</section> <!-- end .page-header  -->
<!--  MAIN CONTENT  -->
<!--  SECTION CAMPAIGNS   -->
<section class="section-content-block" >
    <div class="container">
        <div class="row section-heading-wrapper">
            <div class="col-md-12 col-sm-12 text-center">
                <h2 class="section-heading">Donation Campaigns</h2>
                <p class="section-subheading">Campaigns to encourage new donors to join and existing to continue to give blood.</p>
            </div> <!-- end .col-sm-12  -->                       
        </div> <!-- end .row  -->
        <div class="row">
            <?php foreach ($campaign as $value) { ?>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="event-latest">
                        <div class="row"> 
                            <div class="col-lg-5 col-md-5 hidden-sm hidden-xs">
                                <div class="event-latest-thumbnail">
                                    <a href="#">
                                        <img src="admin/campaign_image/<?php echo $value['campaign_image'];?>" alt="">
                                    </a>
                                </div>
                            </div> <!--  col-sm-5  -->
                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                <div class="event-details">
                                    <a class="latest-date" href="#">
                                        <?php
                                        $str = $value['even_date'];
                                        $dateObj = DateTime::createFromFormat('d-m-Y', $str);
                                        echo $dateObj->format('d M , Y');
                                        ?>
                                    </a>
                                    <h4 class="event-latest-title">
                                        <a href="#"><?php echo $value['even_name'];?></a>
                                    </h4>
                                    <p><?php echo $value['even_title'];?></p>
                                    <div class="event-latest-details">
                                        <a class="author" href="#"><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo $value['even_time'];?></a>
                                        <a class="comments" href="#"> <i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $value['even_venue'];?></a>
                                    </div>
                                </div>
                            </div> <!--  col-sm-7  -->
                        </div>
                    </div>
                </div> <!--  col-sm-6  -->
            <?php } ?>
        </div> <!--  end .row  -->
        <div class="row">
            <div class="col-sm-12 col-md-4 col-md-offset-4 text-center">
                <a class="btn btn-load-more" href="#">Load All Campaigns</a>
            </div>
        </div>
    </div> <!--  end .container  --> 
</section>
<!-- SECTION CTA  -->  