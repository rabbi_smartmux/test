<?php
$singleCampaign = $ob_app->select_all_campaign_info();
$singleValue = mysqli_fetch_assoc($singleCampaign);
?>
<section class="page-header" data-stellar-background-ratio="1.2">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h3>
                    Event Single
                </h3>
            </div>
        </div> <!-- end .row  -->
    </div> <!-- end .container  -->
</section> <!-- end .page-header  -->
<section class="section-content-block">
    <div class="container">
        <div class="row">
            <div class="article-event clearfix">
                <div class="col-sm-12">
                    <article class="post single-post-inner">
                        <div class="post-inner-featured-content">
                            <img alt="" src="admin/campaign_image/<?php echo $singleValue['campaign_image'];?>">
                        </div>
                        <div class="single-post-inner-title">
                            <h2><?php echo $singleValue['even_title'];?></h2>
                            <p class="single-post-meta"><i class="fa fa-user"></i>&nbsp; <?php echo $singleValue['even_title'];?> &nbsp; &nbsp; <i class="fa fa-share"></i>&nbsp; Blood, Save Life</p>
                        </div>
                        <div class="single-post-inner-content">
                            <p><?php echo $singleValue['even_description'];?></p>
                         </div>
                    </article> <!--  end single-post-container --> 
                </div> <!--  end .single-post-container -->
                
            </div>
           
        </div> <!--  end .row  -->
        <div class="row">
            <div class="article-author clearfix">
                <div class="topic-bold-header clearfix">
                    <h4>Event Created by <a href="#"><?php echo $singleValue['even_creator'];?></a></h4>
                </div> <!-- end .topic-bold-header  -->
                <figure class="author-avatar">
                    <a href="#">
                        <img src="admin/creator_images/<?php echo $singleValue['creator_images'];?>" alt="Avatar">
                    </a>
                </figure>
                <div class="about_author">
                   <?php echo $singleValue['creator_opinion'];?>
                </div>

                <div class="social-icons margin-top-11 clearfix">
                    <a class="fa fa-facebook social_icons" href="#" data-original-title="Facebook"></a>
                    <a class="fa fa-twitter social_icons" href="https://twitter.com/#" data-original-title="Twitter"></a>
                    <a class="fa fa-flickr social_icons" href="http://www.flickr.com/photos/#" data-original-title="Flickr"></a>
                    <a class="fa fa-youtube social_icons" href="http://www.youtube.com/#" data-original-title="Youtube"></a>
                    <a class="fa fa-linkedin social_icons" href="#" data-original-title="Linkedin"></a>
                </div>
            </div> <!-- end .article-author  -->
            <div class="post-nav-section clearfix">
                <a class="btn btn-primary fr" href="#">Next Event <i class="fa fa-angle-double-right"></i></a>
                <a class="btn btn-primary" href="#"><i class="fa fa-angle-double-left"></i> Previous Event</a>
            </div> <!-- end .post-nav-section  -->
        </div>
    </div> <!--  end container -->
</section> <!-- end .section-content-block  -->   

<!-- START FOOTER  -->