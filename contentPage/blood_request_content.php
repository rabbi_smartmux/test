<?php
$blood_group = $ob_app->select_all_blood_group_info();
$location = $ob_app->select_all_location_info();

if (isset($_POST['btn'])) {
    $message = $ob_app->save_blood_request_info($_POST);
}
?>
<section class="page-header" data-stellar-background-ratio="1.2">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h3>
                    Blood Request
                </h3>
            </div>
        </div> <!-- end .row  -->
    </div> <!-- end .container  -->
</section> <!-- end .page-header  -->
<!--  MAIN CONTENT  -->
<section class="section-content-block section-contact-block no-bottom-padding">
    <div class="container">
        <div class="row">
            <div class ="col-md-12">
                <div class="row section-heading-wrapper">
                    <div class="col-md-12 col-sm-12 text-center">
                        <h4 style="color: #008f6c; text-align: center">
                            <?php
                            if (isset($_SESSION['message'])) {
                                echo $_SESSION['message'];
                                unset($_SESSION['message']);
                            }
                            ?>
                        </h4>
                        <h2 class="section-heading">Blood Request</h2>
                    </div> <!-- end .col-sm-10  -->   
                </div>  
                <div style="text-align: center;margin-top: 20px;">
                    <p style="color: #008f6c">Please fill the following information to post your blood request. We will inform our donors and we hope the needy people recover soon.</p>
                </div>
            </div>               
            <div class="col-md-12">
                <div style="border: 1px solid;display:block;overflow:auto;padding: 20px;margin-bottom: 40px">
                    <form method="POST" action="">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Patient Name<span style="color: red">*</span></label>
                                <input id="your_name" name="patient_name" class="form-control" placeholder="Patient Name" type="text" required="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Blood Group<span style="color: red">*</span></label>
                                <select name="blood_group" id="inputState" class="form-control" required="">
                                    <option selected>Please Select Blood Group</option>
                                    <?php foreach ($blood_group as $bldgroup) { ?>
                                        <option value="<?php echo $bldgroup['blood_group']; ?>"><?php echo $bldgroup['blood_group']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Patient Age</label>
                                <input id="your_name" name="patient_age" class="form-control" placeholder="Patient Age" type="text">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">How Many Unit You Need<span style="color: red">*</span></label>
                                <input id="your_name" name="blood_qty" class="form-control" placeholder="0.00" type="text" required="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Mobile Number<span style="color: red">*</span></label>
                                <input id="your_name" name="mobile_number" class="form-control" placeholder="Mobile Number" type="text" required="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Line Number</label>
                                <input id="your_name" name="line_number" class="form-control" placeholder="Line Number" type="text">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Email Address</label>
                                <input id="your_name" name="email_address" class="form-control" placeholder="exmple@gmail.com" type="email">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Hospital Name<span style="color: red">*</span></label>
                                <input id="your_name" name="hispital_name" class="form-control" placeholder="Hospital Name" type="text" required="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Location<span style="color: red">*</span></label>
                                <select name="location_id" id="inputState" class="form-control" required="">
                                    <option selected>Please Select Location</option>
                                    <?php foreach ($location as $value) { ?>
                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['location']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Present Address<span style="color: red">*</span></label>
                                <input id="your_name" name="present_address" class="form-control" placeholder="Present Address" type="text" required="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Purpose<span style="color: red">*</span></label>
                                <input id="your_name" name="purpose" class="form-control" placeholder="Purpose" type="text" required="">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">When You Need<span style="color: red">*</span></label>
                            <input id="datepicker" name="need_date" class="form-control"  placeholder="Date" type="text" required="">
                        </div>    
                        <div class="form-group col-md-12 col-sm-12 col-xs-12" align="center">
                            <button name="btn" id="btn_submit" class="btn btn-success" type="submit">Blood Request</button>
                        </div>
                    </form>
                </div>
            </div>
        </div> 
    </div>
</section>
